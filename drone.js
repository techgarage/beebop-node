var stdin = process.stdin;
var bebop = require("node-bebop");


stdin.setRawMode( true );
stdin.resume();
stdin.setEncoding( 'utf8' );

// on any data into stdin
stdin.on( 'data', function( key ){
    drone.land();
    setTimeout(function() {
       process.exit();
    }, 5000);
  }
});

var drone = bebop.createClient();

drone.connect(function() {
  drone.takeOff();

  setTimeout(function() {
    drone.land();
  }, 10000);
});
